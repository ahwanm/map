var searchForm = document.getElementById("searchForm");
const loader = document.querySelector("#loading");

// showing loading
function displayLoading() {
    console.log("Hi");
    loader.classList.add("display");
    // to stop loading after some time
    setTimeout(() => {
        loader.classList.remove("display");
    }, 5000);
}

// hiding loading
function hideLoading() {
    loader.classList.remove("display");
}

searchForm.addEventListener("submit", async function(e){
  e.preventDefault();
  console.log("Hi");
  displayLoading()
  console.log("Bye");
  var city= this["city"].value;
  city=city.split(" ").join("");
  var street= this["street"].value;
  street= street.split(" ").join("");
  var state= this["state"].value;
  state = state.split(" ").join("");
  var zip= this["zip"].value;
  zip = zip.split(" ").join("");
  var country= this["country"].value;
  country = country.split(" ").join("");
  var key = "hgTcEufUNPmfv9YXx5mIvQ2n2BbR2iqs";
  var url = "http://www.mapquestapi.com/geocoding/v1/address?key=" + key + "&street=" + street + "&city=" + city + "&state=" + state + "&postalCode=" + zip;
  console.log(url);
  var x = await fetch(url)
    .then(
        response => response.json()
    );
  var lat = x["results"][0]["locations"][0]["latLng"]["lat"];
  var lng = x["results"][0]["locations"][0]["latLng"]["lng"];
  var w = window.innerWidth;
  var h = window.innerHeight;
  var map = "https://www.mapquestapi.com/staticmap/v5/map?key=hgTcEufUNPmfv9YXx5mIvQ2n2BbR2iqs&locations="+lat+","+lng+"&size="+w+","+h+"&defaultMarker=marker&zoom=14&type=light";
  console.log(map);
  console.log('<img src='+map+'>');
  document.getElementById("map").innerHTML = '<img src=' + map + '>';

  var elevation = "http://open.mapquestapi.com/elevation/v1/profile?key="+ key + "&shapeFormat=raw&unit=f&latLngCollection="+lat+","+lng;
  await fetch(elevation)
    .then(
        response => response.json()
    ).then(
      y=>{
        hideLoading()
        console.log(y);
        e = y["elevationProfile"][0]["height"];
        document.getElementById("elevationresult").innerHTML = "<b>Elevation:</b> " + e;
      }
    );

});
